  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <header>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h2><?php the_title(); ?></h2></a>
    </header>
    <?php
    if ( has_post_thumbnail() ) { ?>
      <figure class="featured-image">
        <?php the_post_thumbnail( 'large' ); ?>
      </figure>
      <?php
    }
    ?>
    <div class="post-content">
      <?php the_excerpt(); ?>
    </div>
    <footer>
      <figure>
        <?php echo get_avatar( get_the_author_meta( 'ID' ), 32 ); ?>
        <figcaption><?php the_author(); ?></figcaption>
      </figure>
      <span><?php the_date( 'Y M d' ); ?></span>
    </footer>
  </article>