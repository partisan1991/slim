  <article id="post-<?php the_ID(); ?>" <?php post_class( 'middle' ); ?>>
    <header>
      <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><h2><?php the_title(); ?></h2></a>
      <span><?php the_author(); ?> &#215; <time datetime="<?php echo get_the_date( 'c' ); ?>" pubdate><?php echo date( get_option( 'date_format', $timestamp ) ); ?></time> &#215; <?php echo get_the_category_list( __( ', ', 'slim' ) ); ?></span>
    </header>
    <?php
    if ( has_post_thumbnail() ) { ?>
      <figure class="featured-image">
        <?php the_post_thumbnail( 'large' ); ?>
      </figure>
      <?php
    }
    ?>
    <div class="post-content">
      <?php the_excerpt(); ?>
    </div>
    <footer></footer>
  </article>