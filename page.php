<?php get_header(); ?>

<?php
while ( have_posts() ) : the_post();
  get_template_part( 'templates/content', 'page' );
endwhile;
?>
<?php wp_link_pages(); ?>
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>