#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-09-07 18:02+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: \n"
"Language: \n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Loco https://localise.biz/"

#: index.php:10
msgid "Sorry, no posts matched your criteria."
msgstr ""

#: inc/core-functions.php:62
msgid "Header menu area"
msgstr ""

#: inc/core-functions.php:63
msgid "Footer menu area"
msgstr ""

#: inc/core-functions.php:80
msgid "Footer"
msgstr ""

#. Name of the theme
msgid "Slim"
msgstr ""

#. Description of the theme
msgid "Streamlined simple Wordpress theme."
msgstr ""

#. URI of the theme
msgid "http://designpositive.hu/slim"
msgstr ""

#. Author of the theme
msgid "Norbert Papp"
msgstr ""

#. Author URI of the theme
msgid "http://designpositive.hu/"
msgstr ""
