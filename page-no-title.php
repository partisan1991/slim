<?php
/*
 * Template Name: Page - No Title
 * Description: Page template without sidebar
 */

get_header(); ?>

<?php
while ( have_posts() ) : the_post();
  get_template_part( 'templates/content', 'page-notitle' );
endwhile;
?>
<?php wp_link_pages(); ?>
<?php wp_reset_postdata(); ?>
<?php get_footer(); ?>